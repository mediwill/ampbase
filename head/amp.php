<!DOCTYPE html>
<html amp lang="ja">
<head>
<meta charset="utf-8">
<script async src="https://cdn.ampproject.org/v0.js"></script>
<script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
<script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>
<?php get_template_part( 'head/amp-tracking-code' ); ?>
<link rel="canonical" href="<?php the_permalink()  ?>">
<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
<?php get_template_part( 'head/title' ); ?>
<?php 
if ( is_single() ) {
	get_template_part( 'head/json-ld' );
}
?>
<style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
<meta name=”amp-google-client-id-api” content=”googleanalytics”>
<?php get_template_part( 'head/style' ); ?>
</head>
<body <?php body_class(); ?>>
