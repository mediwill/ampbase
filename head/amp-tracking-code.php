<?php
$tracking_id = get_theme_mod( 'tracking_id', null );
$amp_tracking_id = get_theme_mod( 'amp_tracking_id', null );
if ( $amp_tracking_id ) {
	$tracking_id = $amp_tracking_id;
} 
if ( !is_user_logged_in() && $tracking_id ) {
	echo '<script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>' . PHP_EOL;
}
