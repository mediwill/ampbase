<?php
// JSON-LD
// https://developers.google.com/search/docs/data-types/articles
// http://schema.org/Article

$author = get_userdata($post->post_author);
?>
<script type="application/ld+json">
[
	{
  	"@context": "https://schema.org",
  	"@type": "<?php echo get_theme_mod('json_ld_type', 'Article'); ?>",
  	"mainEntityOfPage":{
    	"@type":"WebPage",
    	"@id":"<?php the_permalink(); ?>"

  	},
  	"headline": "<?php the_title(); ?>",
		<?php get_template_part( 'head/eyecatch' );	?>
  		"datePublished": "<?php echo (get_the_time('c') ? get_the_time('c') : get_the_modified_time('c')); ?>",
  		"dateModified": "<?php echo get_the_modified_time('c'); ?>",
  		"author": {
    		"@type": "Person",
    		"name": "<?php echo $author->display_name; ?>"
  		},
  		"publisher": {
    		"@type": "Organization",
    		"name": "<?php echo get_theme_mod('json_ld_organization_name', 'My organaization name'); ?>",
<?php get_template_part( 'head/logo' ); ?>
  		},
		"description": "<?php echo ampbase_get_the_description(); ?>"
	},
	<?php get_template_part( 'head/breadcrumb' ); ?>
]
</script>
