<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width">
<?php
if ( ampbase_has_amp_page() ) {
	$link = "<link rel=\"amphtml\" href=\"" . ampbase_get_amp_permalink() . "\">\n";
	echo $link;
}
if ( is_single() ) {
	get_template_part( 'head/json-ld' );
}
get_template_part( 'head/tracking-code' );
get_template_part( 'head/favicon' );
get_template_part( 'head/style' );
wp_head();
?>
</head>
<body <?php body_class(); ?>>
