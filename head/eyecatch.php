"image": {
  			"@type": "ImageObject",
<?php
// get eyecatch url
$image_id = get_post_thumbnail_id( $post->ID );
$image = wp_get_attachment_image_src( $image_id, true );
$image_url = null;
if (isset($image[0])) {
  $image_url = $image[0];
}
$image_file = ampbase_url_to_local( $image_url );
if ( file_exists( $image_file ) ):
  $size = ampbase_get_image_width_and_height( $image_url );
  $width = $size ? $size['width'] : 800;
  $height = $size ? $size['height'] : 800;
  // When the thumbnail width is too small, adjust it to the specification (696px or more).
  if ($width < 696):
    $height = round( $height * ( 696 / $width ) );
    $width = 696;
  endif;
else:
  $image_url = get_template_directory_uri() . '/images/no-image-large.png';
  $width = 800;
  $height = 450;
endif;
?>
    		"url": "<?php echo $image_url;?>",
    		"width": <?php echo $width; ?>,
    		"height": <?php echo $height; ?>

		},
