<?php
	$title = '';
	if ( is_home() || is_front_page() ) {
		$title =  get_bloginfo('name') . ' | ' . get_bloginfo('description');
	} else {
		$title = get_the_title() . ' | ' . get_bloginfo('name');
	}
	echo '<' . 'title>' . $title . '</title>';		// Split title tag for theme checker.
