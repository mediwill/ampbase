<?php
$image_default_url = get_template_directory_uri() . '/images/no-logo.png';
$image_url = get_theme_mod( 'json_ld_logo_url', $image_default_url );
$width = 600;
$height = 60;
?>
    		"logo": {
      			"@type": "ImageObject",
      			"url": "<?php echo $image_url; ?>",
      			"width": <?php echo $width; ?>,
      			"height": <?php echo $height; ?>

    		}
