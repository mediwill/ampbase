<?php
$favicon_url = get_theme_mod('favicon_url', null);
if ($favicon_url) {
	echo '<link rel="shortcut icon" href="' . $favicon_url . '" />';
}
