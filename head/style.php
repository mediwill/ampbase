<?php
if ( WP_Filesystem() ) {
  global $wp_filesystem;

	$reset_css = '';
	$standard_css = '';

  $reset_css_file = get_template_directory() . '/css/reset.css';
  $standard_css_file = get_template_directory() . '/css/standard.css';
  $wordpress_css_file = get_template_directory() . '/css/wordpress.css';

  if ( file_exists( $reset_css_file ) && file_exists( $standard_css_file ) ) {
    $reset_css = $wp_filesystem->get_contents( $reset_css_file );
    $wordpress_css = $wp_filesystem->get_contents( $wordpress_css_file );
    $standard_css = $wp_filesystem->get_contents( $standard_css_file );
  }

	if ( ampbase_is_amp() ) {
		echo '<style amp-custom>';
	} else {
		echo '<style>';
	}

  echo $reset_css;
  echo $wordpress_css;
  echo $standard_css;
	echo '</style>';
}
