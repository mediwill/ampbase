<?php
// Gets the category to which the current post belongs.
$categories = get_the_category();
foreach ( $categories as $category ) {
	echo "{\n";
	echo "	\"@context\": \"http://schema.org\",\n";
	echo "	\"@type\": \"BreadcrumbList\",\n";
	echo "	\"itemListElement\": \n";
	echo "	[\n";
	// Get a list of parent categories.
  $breadcrumb = get_category_parents( $category->term_id, TRUE, ',' );
	$breadcrumb_categories = explode( ',', $breadcrumb, -1 );
	$position = 1;
	foreach ( $breadcrumb_categories as $breadcrumb_category ) {
		preg_match( '@http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?@', $breadcrumb_category, $matches );
		$url = $matches[0];
		$name = strip_tags( $breadcrumb_category );
		echo "		{\n";
		echo "			\"@type\": \"ListItem\",\n";
		echo "			\"position\": " . $position . ",\n"; 
		echo "			\"item\": \n";
		echo "			{\n";
		echo "				\"@id\": \"" . $url . "\",\n";
		echo "				\"name\": \"" , $name . "\"\n";
		echo "			}\n";
		if ( $breadcrumb_category !== end( $breadcrumb_categories ) ) {
			echo "		},\n";
		} else {
			echo "		}\n";
			echo "	]\n";
		}
		$position = $position + 1;
	}
	if ( $category !== end( $categories ) ) {
		echo "	},\n";
		echo "	";
	} else {
		echo "	}\n";
	}
}
