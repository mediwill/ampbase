<?php // Set custom field on posting page

function ampbase_add_custom_boxes(){
  add_meta_box( 'json_ld_setting_in_page', __( 'JSON-LD', 'ampbase' ), 'view_json_ld_box', 'post', 'normal', 'high' );
}
add_action('admin_menu', 'ampbase_add_custom_boxes');

function view_json_ld_box(){
  $json_ld_type = get_post_meta( get_the_ID(), 'json_ld_type', true );
  $json_ld_type = htmlspecialchars( $json_ld_type );

	if ( !$json_ld_type ) {
		$json_ld_type = get_theme_mod( 'json_ld_type', 'Article' );
	}

	?>
	<label>@type:</label>
	<br>
	<input type="text" id="json_ld_type" name="json_ld_type" rows="10" style="margin-top:12px;" value="<?php echo $json_ld_type; ?>
	">
	<?php
}

function ampbase_save_json_ld_type() {
  $id = get_the_ID();

  $json_ld_type = null;
  if ( isset( $_POST['json_ld_type'] ) ){
    $json_ld_type = $_POST['json_ld_type'];
    add_post_meta($id, 'json_ld_type', $json_ld_type, true);
    update_post_meta($id, 'json_ld_type', $json_ld_type);
  }
}
add_action( 'save_post', 'ampbase_save_json_ld_type' );
