<?php
// Is it an AMP page?
if ( !function_exists( 'ampbase_is_amp' ) ):
function ampbase_is_amp(){
  $is_amp = false;

  if ( empty($_GET['amp']) ) {
    return false;
  }

  if ( ampbase_is_amp_enable() && is_single() && $_GET['amp'] === '1' ) {
    $is_amp = true;
  }

  return $is_amp;
}
endif;

// Is it a posting page with an AMP page?
if ( !function_exists( 'ampbase_has_amp_page' ) ):
function ampbase_has_amp_page(){
	$category_ids = get_theme_mod( 'noamp_category_ids', null );
 	$category_ids = explode(",", $category_ids);  // bugfix for simplicity2
	
	// return is_single() && ampbase_is_amp_enable() && !in_category( $category_ids );
	return is_single() && ampbase_is_amp_enable() && !post_is_in_descendant_category( $category_ids );
}
endif;

// Convert css for amp.
if ( !function_exists( 'ampbase_convert_css_for_amp' ) ):
function ampbase_convert_css_for_amp($the_css) {
	if ( !ampbase_is_amp() ) {
		return $the_css;
	}
	$the_css = str_replace('@charset "UTF-8";', '', $the_css);
	return $the_css;
}
endif;

// Convert content for amp.
if ( !function_exists( 'ampbase_convert_content_for_amp' ) ):
function ampbase_convert_content_for_amp($the_content){
  if ( !ampbase_is_amp() ) {
     return $the_content;
  }

	ampbase_convert_iframe( $the_content );

	// C2A0
	$the_content = str_replace('\xc2\xa0', ' ', $the_content);

	// Delete noscrpt tag.
  $the_content = preg_replace('/<noscript>/i','',$the_content);
  $the_content = preg_replace('/<\/noscript>/i','',$the_content);

	// Delete font tag.
  $the_content = preg_replace('/<font[^>]*?>/i','',$the_content);
  $the_content = preg_replace('/<\/font>/i', '', $the_content);

	ampbase_delete_style_attribute( $the_content );

	// Delete target attribute.
  $the_content = preg_replace('/ *?target=["][^"]*?["]/i', '', $the_content);
  $the_content = preg_replace('/ *?target=[\'][^\']*?[\']/i', '', $the_content);

	// Delete onclick attribute.
  $the_content = preg_replace('/ *?onclick=["][^"]*?["]/i', '', $the_content);
  $the_content = preg_replace('/ *?onclick=[\'][^\']*?[\']/i', '', $the_content);

	// Delete onload attribute.
  $the_content = preg_replace('/ *?onload=["][^"]*?["]/i', '', $the_content);
  $the_content = preg_replace('/ *?onload=[\'][^\']*?[\']/i', '', $the_content);

	// Delete marginwidth attribute.
  $the_content = preg_replace('/ *?marginwidth=["][^"]*?["]/i', '', $the_content);
  $the_content = preg_replace('/ *?marginwidth=[\'][^\']*?[\']/i', '', $the_content);

	// Delete marginheight attribute.
  $the_content = preg_replace('/ *? marginheight=["][^"]*?["]/i', '', $the_content);
  $the_content = preg_replace('/ *? marginheight=[\'][^\']*?[\']/i', '', $the_content);

	// Delete font tag.
  $the_content = preg_replace('/<font[^>]+?>/i', '', $the_content);
  $the_content = preg_replace('/<\/font>/i', '', $the_content);

	ampbase_convert_amp_img_tag( $the_content );

  $pattern = '/<p><script.+?<\/script><\/p>/i';
  $append = '';
  $the_content = preg_replace($pattern, $append, $the_content);
  $pattern = '/<script.+?<\/script>/is';
  $append = '';
  $the_content = preg_replace($pattern, $append, $the_content);

  $pattern = '{<amp-img></amp-img>}i';
  $append = '';
  $the_content = preg_replace($pattern, $append, $the_content);

  $pattern = '{<p></p>}i';
  $append = '';
  $the_content = preg_replace($pattern, $append, $the_content);

  return $the_content;
}
endif;
add_filter('the_content','ampbase_convert_content_for_amp', 999999999);

function ampbase_convert_iframe( &$the_content ) {
  /* $the_content = preg_replace("@<iframe(\".*?\"|'.*?'|[^'\"])*?></iframe(\".*?\"|'.*?'|[^'\"])*?>@i", '', $the_content); */

	$amp_placeholder = '<amp-img layout="fill" src="' . get_template_directory_uri() . '/images/transparence.png' . '" placeholder>';
	// amazon
	$pattern = '/<iframe([^>]+?)(src="https?:\/\/rcm-fe.amazon-adsystem.com\/[^"]+?t=([^&"]+)[^"]+?asins=([^&"]+)[^"]*?").*?><\/iframe>/is';
  $amazon_url = 'https://www.amazon.co.jp/exec/obidos/ASIN/$4/$3/ref=nosim/';
  $append = PHP_EOL . '<amp-iframe$1$2 width="120" height="240" frameborder="0">' . $amp_placeholder . '</amp-iframe><br><a href="'. $amazon_url . '" class="aa-link"></a>' . PHP_EOL;
	// youtube
	$the_content = str_replace('http://www.youtube.com/', 'https://www.youtube.com/', $the_content);
	$the_content = preg_replace($pattern, $append, $the_content);
	
  // Corrected to normal link when calling http with title with iframe
  $pattern = '/<iframe[^>]+?src="(http:\/\/[^"]+?)"[^>]+?title="([^"]+?)"[^>]+?><\/iframe>/is';
  $append = '<a href="$1">$2</a>';
  $the_content = preg_replace($pattern, $append, $the_content);
  $pattern = '/<iframe[^>]+?title="([^"]+?)[^>]+?src="(http:\/\/[^"]+?)""[^>]+?><\/iframe>/is';
  $append = '<a href="$1">$2</a>';
  $the_content = preg_replace($pattern, $append, $the_content);
  // Corrected to normal link when calling http in iframe
  $pattern = '/<iframe[^>]+?src="(http:\/\/[^"]+?)"[^>]+?><\/iframe>/is';
  $append = '<a href="$1">$1</a>';
  $the_content = preg_replace($pattern, $append, $the_content);

  // Replace iframe with amp-iframe
  $pattern = '/<iframe/i';
  $append = '<amp-iframe layout="responsive" sandbox="allow-scripts allow-same-origin allow-popups"';
  $the_content = preg_replace($pattern, $append, $the_content);
  $pattern = '/<\/iframe>/i';
  $append = $amp_placeholder.'</amp-iframe>';
  $the_content = preg_replace($pattern, $append, $the_content);
}

// Delete style attribute.
function ampbase_delete_style_attribute( &$the_content ) {
  $the_content = preg_replace('/ *?style=["][^"]*?["]/i', '', $the_content);
  $the_content = preg_replace('/ *?style=[\'][^\']*?[\']/i', '', $the_content);
}

// Convert img tag to amp-img tag.
function ampbase_convert_amp_img_tag ( &$the_content ) {
  $res = preg_match_all('/<img(.+?)\/?>/is', $the_content, $m);
  if ($res) {

    foreach ($m[0] as $match) {
      $src_attr = null;
      $url = null;
      $width_attr = null;
      $width_value = null;
      $height_attr = null;
      $height_value = null;
      $alt_attr = null;
      $alt_value = null;
      $title_attr = null;
      $title_value = null;
      $sizes_attr = null;

			// Get src attribute.
      $src_res = preg_match('/src=["\']([^"\']+?)["\']/is', $match, $srcs);
      if ($src_res) {
        $src_attr = ' '.$srcs[0];
        $url = $srcs[1];
      }

			// Get width attribut.
      $width_res = preg_match('/width=["\']([^"\']*?)["\']/is', $match, $widths);
      if ($width_res) {
        $width_attr = ' '.$widths[0];
        $width_value = $widths[1];
      }
			
			// Get height attribut.
      $height_res = preg_match('/height=["\']([^"\']*?)["\']/is', $match, $heights);
      if ($height_res) {
        $height_attr = ' '.$heights[0];
        $height_value = $heights[1];
      }

			// Get alt attribute.
      $alt_res = preg_match('/alt=["]([^"]*?)["]/is', $match, $alts);
      if (!$alt_res)
        $alt_res = preg_match("/alt=[']([^']*?)[']/is", $match, $alts);
      if ($alt_res) {
        $alt_attr = ' '.$alts[0];
        $alt_value = $alts[1];
      }

			// Get title attribute.
      $title_res = preg_match('/title=["]([^"]*?)["]/is', $match, $titles);
      if (!$title_res)
        $title_res = preg_match("/title=[']([^']*?)[']/is", $match, $titles);
      if ($title_res) {
        $title_attr = ' '.$titles[0];
        $title_value = $titles[1];
      }

			// Get information from images without those with width and height attributes.
      $class_attr = null;
      if ($url && (empty($width_value) || empty($height_value))) {
        $size = ampbase_get_image_width_and_height($url);
        if ($size) {
          $class_attr = ' class="internal-content-img"';
          $width_value = $size['width'];
          $width_attr = ' width="'.$width_value.'"';
          $height_value = $size['height'];
          $height_attr = ' height="'.$height_value.'"';
        } else {
          $class_attr = ' class="external-content-img"';
          $width_value = 300;
          $width_attr = ' width="300"';
          $height_value = 300;
          $height_attr = ' height="300"';
        }
      }

			// Create the size attribute (for clean responsiveness).
      if ($width_value) {
        $sizes_attr = ' sizes="(max-width: '.$width_value.'px) 100vw, '.$width_value.'px"';
      }

			// Create amp-img tag.
      $tag = '<amp-img'.$src_attr.$width_attr.$height_attr.$alt_attr.$title_attr.$sizes_attr.$class_attr.'></amp-img>';

			// Convert img tag to amp-img tag.
      $the_content = preg_replace( '{' . preg_quote($match) . '}', $tag , $the_content, 1 );
    }
  }

	// This is unnecessary, is not it?
  /* $the_content = preg_replace( '/<img(.+?)\/?>/is', '<amp-img$1></amp-img>', $the_content ); */
}

// Get template for amp.
if ( !function_exists( 'ampbase_get_template_part_amp' ) ):
function ampbase_get_template_part_amp($template_name){
  ob_start();
  get_template_part($template_name);
  $template = ob_get_clean();
  $template = ampbase_convert_content_for_amp($template);
  echo $template;
}
endif;

// Get permalink for amp.
if ( !function_exists( 'ampbase_get_amp_permalink' ) ):
function ampbase_get_amp_permalink(){
  $permalink = get_permalink();
  if (strpos($permalink,'?') !== false) {
    $amp_permalink = $permalink.'&amp;amp=1';
  } else {
    $amp_permalink = $permalink.'?amp=1';
  }
  return $amp_permalink;
}
endif;

// Get image width and height.
function ampbase_get_image_width_and_height( $image_url ){
	// The image file must be stored in the local storage.

	// Delete up to "uploads" in url.
	$after_uploads = preg_replace( '/http.*\/wp-content\/uploads/', '', $image_url );

  $wp_upload_dir = wp_upload_dir();
  $uploads_dir = $wp_upload_dir['basedir'];
	$image_file = $uploads_dir . $after_uploads;

	if ( strpos( $image_file, '.svg' ) ) {
		$ret = get_svg_width_and_height( $image_file );
	} else {
		$ret = get_non_svg_width_and_height( $image_file );
	}
	return $ret;
}

// Get width and height of svg file.
function get_svg_width_and_height( $file ) {
	$xml = file_get_contents( $file );

	$width_pattern = '/width="(\d|\.)+"/';
	$height_pattern = '/height="(\d+|\.)+"/';
	$number_pattern = '/\d+/';

	$ret_width = preg_match( $width_pattern, $xml, $width_maches );
	$ret_height = preg_match( $height_pattern, $xml, $height_maches );

	if ( $ret_width && $ret_height ) {
  	$width_attr = $width_maches[0];
  	$height_attr = $height_maches[0];

  	preg_match( $number_pattern, $width_attr, $width_number_maches );
  	preg_match( $number_pattern, $height_attr, $height_number_maches );
  
		$ret = array();
		$ret['width'] = $width_number_maches[0];
		$ret['height'] = $height_number_maches[0];

		return $ret;
	} else {
  	return false;
	}
}

// Get width and height of non svg file.
function get_non_svg_width_and_height( $file ) {
	$imagesize = getimagesize( $file );
  if ( $imagesize ) {
		$ret = array();
    $ret['width'] = $imagesize[0];
    $ret['height'] = $imagesize[1];
    return $ret;
  } else {
		return false;
	}
}
