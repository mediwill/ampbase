<?php
// set theme option
function ampbase_customize_register( $wp_customize ) {
	// ==================== AMP ====================
	$wp_customize->add_section(
		'amp_section',
		array(
		'title' => 'AMP',
		'priority' => 1000,
		)
	);
	// ----------- Amp enable ----------
	$wp_customize->add_setting(
		'amp_enable', 
		array(
			'default' => true,
			'sanitize_callback' => 'sanitize_check',
		)
	);
	$wp_customize->add_control(
		'amp_enable',
		array(
			'settings' => 'amp_enable',
			'label' => __( 'Enable AMP', 'ampbase' ),
			'description' => '',
			'section' => 'amp_section',
			'type' => 'checkbox',
			'priority' => 10,
		)
	);
	// ---------- Amp Tracking ID ----------
	$wp_customize->add_setting(
		'amp_tracking_id', 
		array(
			'default' => '',
			'sanitize_callback' => 'sanitize_text',
		)
	);
	$wp_customize->add_control(
		'amp_tracking_id',
		array(
			'settings' => 'amp_tracking_id',
			'label' => __( 'AMP Tracking ID', 'ampbase' ),
			'description' => __( 'Set if you want to separate AMP tracking.', 'ampbase' ),
			'section' => 'amp_section',
			'type' => 'text',
			'priority' => 10,
		)
	);
	// ---------- The Categories that disable AMP ----------
	$wp_customize->add_setting(
		'noamp_category_ids', 
		array(
			'default' => '',
			'sanitize_callback' => 'sanitize_id_comma_text',
		)
	);
	$wp_customize->add_control( 
		'noamp_category_ids', 
		array(
			'settings' => 'noamp_category_ids',
			'label' => __( 'Set the categories that disable AMP.', 'ampbase' ),
			'description' =>	__( 'Example: 125,8116', 'ampbase' ),
			'section' => 'amp_section',
			'type' => 'text',
			'priority'=> 60,
		)
	);
	// ==================== JSON-LD ====================
	$wp_customize->add_section(
		'json_ld_section',
		array(
			'title' => 'JSON-LD',
			'priority' => 2000,
		)
	);
	// ----------	@type ----------
	$wp_customize->add_setting(
		'json_ld_type', 
		array(
			'default' => 'Article',
			'sanitize_callback' => 'sanitize_text',
		)
	);
	$wp_customize->add_control(
		'json_ld_type',
		array(
			'settings' => 'json_ld_type',
			'label' => __( '@type', 'ampbase' ),
			'description' => 'See -> http://schema.org/docs/full.html',
			'section' => 'json_ld_section',
			'type' => 'text',
			'priority' => 10,
		)
	);
	// ---------- Logo ----------
	$wp_customize->add_setting(
		'json_ld_logo_url', 
		array(
			'default' => '/images/no-logo.png',
			'sanitize_callback' => 'sanitize_file_url',
		)
	);
	$wp_customize->add_control( 
		new \WP_Customize_Image_Control( 
			$wp_customize,
			'json_ld_logo_url',
			array(
				'settings' => 'json_ld_logo_url',
				'label' => __( 'Logo URL', 'ampbase' ),
				'description' => 'size: 600x60',
				'section' => 'json_ld_section',
				'priority' => 20,
			)
		)
	);
	// ---------- Publisher ----------
	$wp_customize->add_setting(
		'json_ld_organization_name', 
		array(
			'default' => '',
			'sanitize_callback' => 'sanitize_text',
		)
	);
	$wp_customize->add_control(
		'json_ld_organization_name',
		array(
			'settings' => 'json_ld_organization_name',
			'label' => __( 'Organization name', 'ampbase' ),
			'description' => '',
			'section' => 'json_ld_section',
			'type' => 'text',
			'priority' => 30,
		)
	);
	// ==================== Analytics ====================
	$wp_customize->add_section(
		'analytics_section',
		array(
			'title' => 'Analytics',
			'priority' => 3000,
		)
	);
	$wp_customize->add_setting(
		'tracking_id', 
		array(
			'default' => '',
			'sanitize_callback' => 'sanitize_text',
		)
	);
	$wp_customize->add_control(
		'tracking_id',
		array(
			'settings' => 'tracking_id',
			'label' => __( 'Tracking ID', 'ampbase' ),
			'description' => '',
			'section' => 'analytics_section',
			'type' => 'text',
			'priority' => 10,
		)
	);
	// ==================== SVG Header Image ====================
	$wp_customize->add_section(
		'svg_header_image_section',
		array(
			'title' => 'SVG Header Image',
			'priority' => 4000,
		)
	);
	// ---------- Logo ----------
	$wp_customize->add_setting(
		'svg_header_image', 
		array('sanitize_callback' => 'sanitize_file_url',)
	);
	$wp_customize->add_control( 
		new \WP_Customize_Image_Control( 
			$wp_customize,
			'svg_header_image',
			array(
				'settings' => 'svg_header_image',
				'label' => __( 'SVG Header Image URL', 'ampbase' ),
				'description' => 'No margin is given to the image file itself. The recommended size of the image is 800 x 200.',
				'section' => 'svg_header_image_section',
				'priority' => 10,
			)
		)
	);
	// ==================== Header Articles ====================
	$wp_customize->add_section(
		'header_articles_section',
		array(
			'title' => 'Header Articles',
			'priority' => 5000,
		)
	);
	$wp_customize->add_setting(
		'header_articles', 
		array(
			'default' => '',
			'sanitize_callback' => 'sanitize_text',
		)
	);
	$wp_customize->add_control(
		'header_articles',
		array(
			'settings' => 'header_articles',
			'label' => __( 'Header Articles', 'ampbase' ),
			'description' => '',
			'section' => 'header_articles_section',
			'type' => 'text',
			'priority' => 10,
		)
	);

	// ==================== Disable Auto Insert of <p> ====================
	$wp_customize->add_section(
		'disable_auto_insert',
		array(
			'title' => 'Disable Auto Insert of <p>',
			'priority' => 6000,
		)
	);
	// ---------- ID that disable auto insert	----------
	$wp_customize->add_setting(
		'id_disable_auto_insert', 
		array(
			'default' => '',
			'sanitize_callback' => 'sanitize_id_comma_text',
		)
	);
	$wp_customize->add_control( 
		'id_disable_auto_insert', 
		array(
			'settings' => 'id_disable_auto_insert',
			'label' => __( 'Specify ID that disable auto insert of <p>.', 'ampbase' ),
			'description' => __( 'Example: 6077, 6437, 6580 ', 'ampbase' ),
			'section' => 'disable_auto_insert',
			'type' => 'textarea',
			'priority'=> 10,
		)
	);
	// ----------- Disable all auto insert ----------
	$wp_customize->add_setting(
		'disable_all_auto_insert', 
		array(
			'default' => false,
			'sanitize_callback' => 'sanitize_check',
		)
	);
	$wp_customize->add_control(
		'disable_all_auto_insert',
		array(
			'settings' => 'disable_all_auto_insert',
			'label' => __( 'Disable all auto insert of <p>', 'ampbase' ),
			'description' => __( 'Disable all regardless of ID specification.', 'ampbase' ),
			'section' => 'disable_auto_insert',
			'type' => 'checkbox',
			'priority' => 20,
		)
	);

	// ==================== Others ====================
	$wp_customize->add_section(
		'others_section',
		array(
			'title' => __( 'Others', 'ampbase' ),
			'description' => __( 'Fabicon etc.', 'ampbase' ),
			'priority' => 7000,
		)
	);
	// Favicon
	$wp_customize->add_setting(
		'favicon_url', 
		array(
			'sanitize_callback' => 'sanitize_file_url',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'favicon_url', 
			array(
				'settings' => 'favicon_url',
				'label' => __( 'favicon', 'ampbase' ),
				'description' => __( 'You can set favicon from media.', 'ampbase' ),
				'section' => 'others_section',
				'priority' => 10,
			)
		) 
	);
	// ----------- Show Site Description ----------
	$wp_customize->add_setting(
		'show_site_description', 
		array(
			'default' => false,
			'sanitize_callback' => 'sanitize_check',
		)
	);
	$wp_customize->add_control(
		'show_site_description',
		array(
			'settings' => 'show_site_description',
			'label' => __( 'Show Site Description', 'ampbase' ),
			'description' => 'Show the description of the site under the site image.',
			'section' => 'others_section',
			'type' => 'checkbox',
			'priority' => 20,
		)
	);

	// Copylight
	$wp_customize->add_setting( 'copylight', array(
	'sanitize_callback' => 'sanitize_text',
	) );
	$wp_customize->add_control(
		'copylight',
		array(
			'settings' => 'copylight',
			'label' => __( 'Copylight', 'ampbase' ),
			'description' => 'Example: 2014-2017 Example Co., Ltd.',
			'section' => 'others_section',
			'priority' => 30,
		)
	);
}
add_action( 'customize_register', 'ampbase_customize_register' );

// is amp enable
function ampbase_is_amp_enable(){
	return get_theme_mod( 'amp_enable', false );
}
