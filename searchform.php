<form role="search" method="get" class="searchform" action="<?php echo home_url( '/' ); ?>">
	<input type="text" placeholder="<?php echo __( 'Enter keyword', 'ampbase' ); ?>" value="" name="s" class="searchtext" />
  <input type="submit" class="searchbutton" value="<?php echo __( "search", 'ampbase' ); ?>" />
</form>

