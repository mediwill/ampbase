<?php
class AMPBaseArchive extends WP_Widget {
	function __construct() {
		parent::__construct(
			'archive_list',
			'AMPBase Archive List',
			array('description' => __('Archive Title', 'ampbase' ) )
		);
	}

	function widget( $args, $instance ) {
		$archive_title = $instance['archive_title'];
		$archive_limit = intval( $instance['archive_limit'] );
		?>
			<div class="wrap">
				<p><?php echo $archive_title; ?></p>	
			</div>
			<div class='menu-wrap'>
				<ul class="archive-list">
				<?php wp_get_archives( array( 'limit' => $archive_limit ) ); ?>
				</ul>
			</div>
		<?php
	}

	function form( $instance ) {
		$defaults = array(
			'archive_title' => __( 'Archive', 'ampbase' ),
			'archive_limit' => 12
		);
		$instance = wp_parse_args( (array)$instance, $defaults );
		$archive_title = $instance['archive_title'];
		$archive_limit = $instance['archive_limit'];
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'archive_title' ); ?>"><?php _e('Title: ', 'ampbase' ); ?></label> 
		<input class="widefat"
			id="<?php echo $this->get_field_id( 'archive_title' ); ?>"
			name="<?php echo $this->get_field_name( 'archive_title' ); ?>"
			type="text" value="<?php echo esc_attr( $archive_title ); ?>"
		>
		</p>
		<p>
		<label for="<?php echo $this->get_field_id( 'archive_limit' ); ?>"><?php _e('Archive Limit: ', 'ampbase' ); ?></label> 
		<input class="small-text"
			id="<?php echo $this->get_field_id( 'archive_limit' ); ?>"
			name="<?php echo $this->get_field_name( 'archive_limit' ); ?>"
			type="text" value="<?php echo esc_attr( $archive_limit ); ?>"
		>
		</p>
		<?php
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['archive_title']  = sanitize_text_field( $new_instance['archive_title'] );
		$instance['archive_limit']  = sanitize_text_field( $new_instance['archive_limit'] );
		return $instance;
	}
}
