<?php
class AMPBaseMenu extends WP_Widget {
	function __construct() {
		parent::__construct( 
			'ampbase_menu', 
			'AMPBase Menu',
			array('description' => __('Set title and location', 'ampbase' ) )
		);
	}

	function widget( $args, $instance ) {
		$menu_title = $instance['menu_title'];
		$menu_name = $instance['menu_name'];
		$two_columns = $instance['two_columns'];
		$menu_class = 'nav-menu';
		if ( $menu_title ) {
			?>
			<div class="wrap menu-title">
			<h2><?php echo $menu_title; ?></h2>
			</div>
			<?php
		}
		
		if ($two_columns == 'two_columns') {
			$menu_class = 'two-columns';
			echo '<div class="menu-wrap two-columns-wrap">';
		} else {
			echo '<div class="menu-wrap">';
		}

		$menu_items = wp_get_nav_menu_items( $menu_name );
		foreach ( (array) $menu_items as $key => $menu_item ) {
      $title = $menu_item->title;
      $url = $menu_item->url;
			$classes = $menu_item->classes;
			$class_str = '';
			foreach ( (array) $classes as $class ) {
				$class_str = $class_str . ' ' . $class;
			} 
    	echo '<a href="' . $url . '"><div class="' . $menu_class . $class_str . '"><span>' . $title . '</span></div></a>';
		}
		echo '</div><!-- menu-wrap  -->';
	}

	function form( $instance ) {
		$defaults = array(
			'menu_title' => __( 'Feature', 'ampbase' ),
			'menu_location' => 'nav-menu-1'
		);
		$instance = wp_parse_args( (array)$instance, $defaults );
		$menu_title = $instance['menu_title'];
		$menu_name = $instance['menu_name'];
		$two_columns = $instance['two_columns'];
		$nav_menus = wp_get_nav_menus();
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'menu_title' ); ?>"><?php _e( 'Title: ', 'ampbase' ); ?></label> 
			<input class="widefat"
				id="<?php echo $this->get_field_id( 'menu_title' ); ?>"
				name="<?php echo $this->get_field_name( 'menu_title' ); ?>"
				type="text" value="<?php echo esc_attr( $menu_title ); ?>"
			>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'menu_name' ); ?>"><?php _e( 'Menu Name: ', 'ampbase' ); ?></label> 
		<?php
			if ( count( $nav_menus ) > 0 ) {
				$html .= '<select name="' . $this->get_field_name( 'menu_name' )  . '" id="' . $this->get_field_id( 'menu_name' )  . '">';
				foreach ( (array) $nav_menus as $menu ) {
					if ( $menu_name === $menu->name ) {
						$html .= '<option value="' . esc_attr( $menu->name ) . '" selected>';
					} else {
						$html .= '<option value="' . esc_attr( $menu->name ) . '">';
					}
					$html .= esc_html( $menu->name );
					$html .= '</option>';
				}
				$html .= '</select>';
			}
			echo $html;
		?>
		</p>
		<p>
			<input
				id="<?php echo $this->get_field_id( 'two_columns' ); ?>"
				name="<?php echo $this->get_field_name( 'two_columns' ); ?>"
				type="checkbox" value="two_columns"
				<?php if ( $two_columns === 'two_columns' ) echo 'checked="checked"'; ?>
			>
			<?php _e( 'Two Columns', 'ampbase' ); ?>
			<br>
		</p>
		<?php
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		$instance['menu_title']  = sanitize_text_field( $new_instance['menu_title'] );
		$instance['menu_name']  = sanitize_text_field( $new_instance['menu_name'] );
		$instance['two_columns']  = sanitize_text_field( $new_instance['two_columns'] );
	
		/*	
		$menu_location = $new_instance['menu_location'];
		if ($menu_location == 'nav-menu-1') {
			$instance['menu_location'] = 'nav-menu-1';
		} elseif ($menu_location == 'nav-menu-2' ) {
			$instance['menu_location'] = 'nav-menu-2';
		} else {
			$instance['menu_location'] = 'main-menu';
		}
		*/
		return $instance;
	}

}
