<?php
class AMPBaseArticles extends WP_Widget {
	function __construct() {
		parent::__construct( 
			'ampbase_articles', 
			'AMPBase Articles',
			array('description' => __('Set title and location', 'ampbase' ) )
		);
	}

	function widget( $args, $instance ) {
		$menu_title = $instance['menu_title'];
		$menu_name = $instance['menu_name'];
		if ( $menu_title ) {
			?>
			<div class="wrap menu-title">
			<h2><?php echo $menu_title; ?></h2>
			</div>
			<?php
		}

		?>		
		<ul class="article-list">
		<?php

		$menu_items = wp_get_nav_menu_items( $menu_name );
		foreach ( (array) $menu_items as $key => $menu_item ) {
      $title = $menu_item->title;
      $url = $menu_item->url;
			$object_id = $menu_item->object_id;

			?>
			<li>
				<a href="<?php echo $url; ?>">
					<div class="article-record">
						<div class="thumbnail-culumn">
    					<?php
				        if ( ampbase_is_amp() ) {
									$thumbnail = get_the_post_thumbnail( $object_id, array(150, 150) );
									ampbase_convert_amp_img_tag($thumbnail);
									echo $thumbnail;
								} else {
									echo get_the_post_thumbnail( $object_id, array(150, 150) );
								}
							?>
						</div>
						<div class="title-culumn">
    					<div class="article-title"><?php echo $title; ?></div>
						</div>
					</div><!-- article-record -->
				</a>
			</li>
			<?php
		}
	
		?>
		</ul>
		<?php
	}

	function form( $instance ) {
		$defaults = array(
			'menu_title' => __( 'Feature', 'ampbase' ),
			'menu_location' => 'nav-menu-1'
		);
		$instance = wp_parse_args( (array)$instance, $defaults );
		$menu_title = $instance['menu_title'];
		$menu_name = $instance['menu_name'];
		$nav_menus = wp_get_nav_menus();
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'menu_title' ); ?>"><?php _e( 'Title: ', 'ampbase' ); ?></label> 
		<input class="widefat"
			id="<?php echo $this->get_field_id( 'menu_title' ); ?>"
			name="<?php echo $this->get_field_name( 'menu_title' ); ?>"
			type="text" value="<?php echo esc_attr( $menu_title ); ?>"
		>
		</p>
		<p>
		<label for="<?php echo $this->get_field_id( 'menu_name' ); ?>"><?php _e( 'Menu Name: ', 'ampbase' ); ?></label> 
		<?php
		if ( count( $nav_menus ) > 0 ) {
			// $html .= '<select name="option[' . "menu_name" . ']" id="menu_name">';
			$html .= '<select name="' . $this->get_field_name( 'menu_name' )  . '" id="' . $this->get_field_id( 'menu_name' )  . '">';
			foreach ( (array) $nav_menus as $menu ) {
				if ( $menu_name === $menu->name ) {
					$html .= '<option value="' . esc_attr( $menu->name ) . '" selected>';
				} else {
					$html .= '<option value="' . esc_attr( $menu->name ) . '">';
				}
				$html .= esc_html( $menu->name );
				$html .= '</option>';
			}
			$html .= '</select>';
		}
		echo $html;
		?>
		</p>
		<?php
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		$instance['menu_title']  = sanitize_text_field( $new_instance['menu_title'] );
		$instance['menu_name']  = sanitize_text_field( $new_instance['menu_name'] );
	
		return $instance;
	}

}
