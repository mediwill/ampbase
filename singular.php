<?php
if ( ampbase_is_amp() ) {
	get_template_part( 'head/amp' );
} else {
	get_template_part( 'head/standard' );
}
get_template_part( 'body/body' );
