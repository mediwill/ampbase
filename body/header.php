<header>
	<div class="body-header">
	<?php
		get_template_part( 'body/top-image' );
		$desc = get_bloginfo( 'description' );
		if ( $desc && get_theme_mod('show_site_description') ) {
			echo '<div class="blog-desc">';
			echo $desc;
			echo '</div>';
		}
		if ( !ampbase_is_amp() && is_home() ) {
				get_search_form();
		}
		if ( is_home() ) {
			get_template_part( 'body/header-articles' );
		}
		if ( is_home() ) get_template_part( 'body/main-menu' );
	?>
	</div>
</header>
