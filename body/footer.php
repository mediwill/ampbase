		<footer>
			<div class="body-footer">
				<?php
					get_template_part( 'body/top-image' );
        	if ( !ampbase_is_amp() ) get_search_form();
				?>
				<div class="footer-menu-wrap">
				<?php
					dynamic_sidebar( 'footer-menu' );
				?>
				</div><!-- footer-menu-wrap -->
				<?php
					$copyright = get_theme_mod( 'copylight', null );
					if ( $copyright ) {
						echo '<div class="copyright">&copy; ' . $copyright . '</div>';
					}
				?>
			</div><!-- body-footer -->
		</footer>
		<?php wp_footer(); ?>
	</body>
</html>
