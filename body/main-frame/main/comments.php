<?php
// Display commnets.
wp_list_comments();
// Display pagination link for comments.
paginate_comments_links();
// Display comment reply.
wp_enqueue_script( 'comment-reply' );
// Display commnet form/
comment_form();
