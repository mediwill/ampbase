<?php
$str = get_theme_mod( 'header_articles' );
$article_ids = explode( ',', $str );
$first_article_id = trim($article_ids[0]);

$title = get_the_title( $first_article_id );
$url = get_permalink( $first_article_id );

if ($first_article_id && !is_paged() ) : 
?>
	<a href="<?php echo $url; ?>">
	<div class="header-article">
		<?php echo get_the_post_thumbnail( $first_article_id, array(800, 450) ); ?>
		<div class="header-article-title">
			<h1><?php echo $title; ?></h1>
		</div>
	</div>
	</a>
<?php
endif;


