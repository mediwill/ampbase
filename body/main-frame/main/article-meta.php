<div class="article-meta">
	<?php // $avatar = get_avatar(); ?>
	<time datetime="<?php echo get_the_date('Y-m-d'); ?>">
		<?php
			echo get_the_date('Y/n/j') . __( 'published', 'ampbase' );
		?>
	</time>
	<span class="date">
		<?php echo get_the_modified_date('Y/n/j') . __( 'updated', 'ampbase' ); ?>
	</span>
	<?php
  	if ( function_exists ( 'wpp_get_views' ) ) {
    	echo '<span class="views">' .  wpp_get_views ( get_the_ID() ) . __( 'views', 'ampbase' ) . '</span>';
    }
	?>
</div>
