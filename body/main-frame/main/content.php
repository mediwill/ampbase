<?php
while (have_posts()): the_post();
?>
	<article>
		<header>
			<div class="wrap">
			<?php get_template_part('body/main-frame/main/breadcrumb'); ?>
			</div>
			<h1><?php the_title(); ?></h1>
			<?php
				if ( ampbase_is_amp() ) {
					ob_start();
					the_post_thumbnail( 'full' );
					$post_thumbnail = ob_get_contents();
					ob_end_clean();
					ampbase_convert_amp_img_tag($post_thumbnail);
					echo $post_thumbnail;
				} else {
					the_post_thumbnail( 'full' );
				}
			?>
		</header>
		<div class="article-body">
		<?php
			if ( has_excerpt() ) {
				if ( ampbase_is_amp() ) {
					ob_start();
					the_excerpt();
					$excerpt = ob_get_contents();
					ob_end_clean();
					ampbase_convert_amp_img_tag( $excerpt );
					echo $excerpt;
				} else {
					the_excerpt();
				}
			}
			the_content();
			wp_link_pages();
		?>
		<div><!-- article-body  -->
		<footer>
		<?php
			if ( is_single() ) {
				get_template_part( 'body/main-frame/main/article-meta' );
				echo the_tags( '<div class="wrap tag-wrap">', ' ', '</div>' ); 
			}
		?>
		</footer>
	</article>
<?php
	if ( !ampbase_is_amp() ) comments_template( '/body/main-frame/main/comments.php' );
endwhile;

