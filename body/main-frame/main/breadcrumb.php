<?php
$categories = get_the_category();
foreach ( $categories as $category ) {
	echo '<div class="breadcrumb">';
	$separater = '<span class="breadcrumb-separater">/</span>';
	$breadcrumb = get_category_parents($category->term_id, TRUE, $separater);
	// Delete last slash
	$pattern = '/\<span class="breadcrumb-separater"\>\/\<\/span\>$/';
	$breadcrumb = preg_replace( $pattern, '', $breadcrumb );
	echo $breadcrumb;
	echo '</div>';
}
