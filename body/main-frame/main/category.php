<?php
	// Categoty
	$category_id = get_query_var( 'cat' );
	$category = get_category($category_id);
	$category_str = __( 'category: ', 'ampbase' ) . $category->name;
	echo( '<div class="wrap">' . $category_str . '</div>' );
	if ( category_description() ) {
		echo( '<div class="wrap">' . category_description() . '</div>' );
	}
	get_template_part( 'body/main-frame/main/loop' );
