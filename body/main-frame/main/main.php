<main>
<?php
if ( is_singular() ) {
	// Singular
	get_template_part( 'body/main-frame/main/content' );
} elseif ( is_404() ) {
	// 404
	?>
	<article>
		<p><?php echo __( 'There is no page.', 'ampbase' ); ?></p>
	</article>
	<?php
} elseif ( is_home() ) {
	// Home
	get_template_part( 'body/main-frame/main/top-articles' );
	echo( '<div class="wrap menu-title">' . __( 'Latest article', 'ampbase' ) . '</div>' );
	get_template_part( 'body/main-frame/main/loop' );
} elseif ( is_category() ) {
	// Categoty
	get_template_part( 'body/main-frame/main/category' );
} elseif ( is_tag() ) {
	// Tag
	$tag_id = get_query_var( 'tag_id' );
	$tag = get_tag( $tag_id );
	$tag_str = __( 'tag: ', 'ampbase' ) . $tag->name;
	echo( '<div class="wrap">' . $tag_str . '</div>' );
	get_template_part( 'body/main-frame/main/loop' );
} elseif ( is_date() ) {
	// Date
	// single_month_title();
	echo( '<div class="wrap">' . get_the_archive_title() . '</div>' );
	get_template_part( 'body/main-frame/main/loop' );
} elseif ( is_search() ) {
  // Search
  $search_query =  get_search_query();
  ?>
  <form role="search" method="get" class="searchform" action="<?php echo home_url( '/' ); ?>">
    <input type="text" placeholder="<?php echo __( 'Enter keyword', 'ampbase' ); ?>" value="<?php echo $search_query; ?>" name="s" class="searchtext" />
    <input type="submit" class="searchbutton" value="<?php echo __( "search", 'ampbase' ); ?>" />
  </form>
  <?php
  $message = "";
  if ( !have_posts() ) {
    $message = '<div class="wrap">' . __( 'No results.', 'ampbase' ) . '</div>';
  } else {
    $message = '<div class="wrap">' . $wp_query->found_posts . __( ' results.', 'ampbase' ) . '</div>';
  }
  echo $message;
	// echo '<div class="wrap">' . __( 'keyword: ', 'ampbase' )  . get_search_query() . '</div>';
	get_template_part( 'body/main-frame/main/loop' );
}
?>
</main>
