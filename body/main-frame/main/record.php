<li>
	<a href="<?php the_permalink(); ?>">
	<article>
		<div class="thumbnail-culumn">
    	<?php
				if ( has_post_thumbnail() ) {
					the_post_thumbnail( array(150, 150) );
    		}
			?>
		</div>
		<div class="title-culumn">
			<!-- div -->
    		<div class="article-title">
					<?php the_title(); ?>
				</div>
				<div class="article-meta" >
					<?php
						echo '<span class="date">' . get_the_modified_date('Y/n/j') . __( 'updated', 'ampbase' ) . '</span>';
						if ( function_exists ( 'wpp_get_views' ) ) {
							echo '<span class="views">' .  wpp_get_views ( get_the_ID() ) . __( 'views', 'ampbase' ) . '</span>';
						}
					?>
				</div>
			</div>
		<!-- /div -->
	</article><!-- article -->
	</a>
</li>
