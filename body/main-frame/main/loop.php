<ul class="article-list">
<?php
while ( have_posts() ){
	the_post();
	get_template_part( 'body/main-frame/main/record' );
} 
?>
</ul>
<div class="pagination">
<?php
	get_template_part( 'body/main-frame/main/pagination' );
?>
</div>
