<nav>
<?php
	// Top Ad
	if ( is_singular() ) {
		if ( ampbase_is_amp() ) {
			if ( is_active_sidebar( 'singular-nav-top-amp-ad' ) ) {
				echo '<div class="ad">';
				dynamic_sidebar( 'singular-nav-top-amp-ad' );
				echo '</div>';
			}
		} else {
			if ( is_active_sidebar( 'singular-nav-top-ad' ) ) {
				echo '<div class="ad">';
				dynamic_sidebar( 'singular-nav-top-ad' );
				echo '</div>';
			}
		}
	}
	if ( is_category() ) {
		if ( ampbase_is_amp() ) {
			/*
			if ( is_active_sidebar( 'category-nav-top-amp-ad' ) ) {
				echo '<div class="ad">';
				dynamic_sidebar( 'category-nav-top-amp-ad' );
				echo '</div>';
			}
			*/
		} else {
			if ( is_active_sidebar( 'category-nav-top-ad' ) ) {
				echo '<div class="ad">';
				dynamic_sidebar( 'category-nav-top-ad' );
				echo '</div>';
			}
		}
	}
	// Widget
	if ( is_home() ) {
		dynamic_sidebar( 'home-nav-widget' );
	} else {
		dynamic_sidebar( 'nav-widget' );
	}
	// Bottom Ad
	if ( is_singular() ) {
		if ( ampbase_is_amp() ) {
			if ( is_active_sidebar( 'singular-nav-bottom-amp-ad' ) ) {
				echo '<div class="ad">';
				dynamic_sidebar( 'singular-nav-bottom-amp-ad' );
				echo '</div>';
			}
		} else {
			if ( is_active_sidebar( 'singular-nav-bottom-ad' ) ) {
				echo '<div class="ad">';
				dynamic_sidebar( 'singular-nav-bottom-ad' );
				echo '</div>';
			}
		}
	}
	if ( is_category() ) {
		if ( ampbase_is_amp() ) {
			/*
			if ( is_active_sidebar( 'category-nav-bottom-amp-ad' ) ) {
				echo '<div class="ad">';
				dynamic_sidebar( 'category-nav-bottom-amp-ad' );
				echo '</div>';
			}
			*/
		} else {
			if ( is_active_sidebar( 'category-nav-bottom-ad' ) ) {
				echo '<div class="ad">';
				dynamic_sidebar( 'category-nav-bottom-ad' );
				echo '</div>';
			}
		}
	}
?>
</nav>
