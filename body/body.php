<?php
if ( ampbase_is_amp() ) get_template_part( 'body/amp-tracking-code' );
get_template_part( 'body/header' );
get_template_part( 'body/main-frame/main-frame' );
get_template_part( 'body/footer' );
