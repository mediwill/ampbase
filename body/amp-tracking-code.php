<?php
if ( !is_user_logged_in() ):
  $tracking_id = get_theme_mod( 'tracking_id', null );
	$amp_tracking_id = get_theme_mod( 'amp_tracking_id', null );
	if ( $amp_tracking_id) {
		$tracking_id = $amp_tracking_id;
	} 
  $after_title = '[AMP]';
  if ( ampbase_is_amp() && $tracking_id ) :
?>
  <amp-analytics type="googleanalytics" id="analytics1">
  <script type="application/json">
  {
    "vars": {
      "account": "<?php echo $tracking_id ?>"
    },
    "triggers": {
      "trackPageviewWithAmpdocUrl": {
        "on": "visible",
        "request": "pageview",
        "vars": {
          "title": "<?php the_title() ?><?php echo $after_title; ?>",
          "ampdocUrl": "<?php echo ampbase_get_amp_permalink() ?>"
        }
      }
    }
  }
  </script>
  </amp-analytics>
<?php
	endif;
endif;

