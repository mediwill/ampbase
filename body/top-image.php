<div class="top">
<a href="/"><?php
$image_path = '';
$height = 200;
$width = 800;

$svg_image_path = get_theme_mod( 'svg_header_image' );

// Prioritize SVG images.
if ( $svg_image_path ) {
	$image_path = $svg_image_path;
	// $size = ampbase_get_image_width_and_height( $url );
	$size = ampbase_get_image_width_and_height ($image_path );
  if ( $size ) {
  	$width = $size['width'];
    $height = $size['height'];
  }
} else {
	$image_path = get_header_image();
	$height = get_custom_header()->height;
	$width = get_custom_header()->width;
}

$blog_name = get_bloginfo('name');

if( $image_path ) {
	if ( ampbase_is_amp() ) {
		echo '<amp-img src="' . $image_path . '" height="' . $height . '" width="' . $width . '" alt="' . $blog_name . '" layout="responsive"></amp-img>';
	} else {
		echo '<img src="' . $image_path . '" alt="' . $blog_name . '"/>';
	}
} else {
	echo '<div class="wrap">' . $blog_name  . '</div>';
}?></a>
</div>
