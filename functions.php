<?php

// Use WP_Filesystem.
require_once( ABSPATH . 'wp-admin/includes/file.php' );

include 'inc/amp.php';
include 'inc/sanitize.php';
include 'inc/customizer.php';
include 'inc/custom-field.php';
include 'widgets/archive.php';
include 'widgets/menu.php';
include 'widgets/articles.php';

// Set the width of the content.
if ( ! isset( $content_width ) ) $content_width = 800;

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
if ( !function_exists( 'ampbase_theme_setup' ) ):
function ampbase_theme_setup() {
	// Textdomain 
	load_theme_textdomain( 'ampbase', get_template_directory() . '/languages' );

	// Theme support
	add_theme_support( 'title-tag' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'post-thumbnails' );

	// Menu
	/*
	register_nav_menu( 'main-menu', __( 'Main Menu', 'ampbase') );
	register_nav_menu( 'nav-menu-1', __( 'Nav Menu 1', 'ampbase' ) );
	register_nav_menu( 'nav-menu-2', __( 'Nav Menu 2', 'ampbase' ) );
	register_nav_menu( 'nav-menu-3', __( 'Nav Menu 3', 'ampbase' ) );
	register_nav_menu( 'nav-menu-4', __( 'Nav Menu 4', 'ampbase' ) );
	*/
	register_nav_menu( 'footer-menu-1', __( 'Footer Menu 1', 'ampbase' ) );
	register_nav_menu( 'footer-menu-2', __( 'Footer Menu 2', 'ampbase' ) );

	// Widget
	register_widget( 'AMPBaseArchive' );
	register_widget( 'AMPBaseMenu' );
	register_widget( 'AMPBaseArticles' );

	// filter
	// remove_filter ( 'the_content', 'wpautop' );
}
endif;
add_action( 'after_setup_theme', 'ampbase_theme_setup' );

// Check site url is included.
if ( !function_exists( 'ampbase_includes_site_url' ) ):
function ampbase_includes_site_url($url){
  if (strpos($url, site_url()) === false) {
    return false;
  } else {
    return true;
	}
}
endif;

// Change url to local path.
if ( !function_exists( 'ampbase_url_to_local' ) ):
function ampbase_url_to_local($url){
	/*
  if (!ampbase_includes_site_url($url)) {
    return false;
  }
	*/
  // $path = str_replace(content_url(), WP_CONTENT_DIR, $url);
  $path = preg_replace('/http.*wp-content/', WP_CONTENT_DIR, $url);
	// echo "<meta>path: $path</meta>";
  // $path = str_replace('\\', '/', $path);
  return $path;
}
endif;

// Get the description.
if ( !function_exists( 'ampbase_get_the_description' ) ):
function ampbase_get_the_description(){
	$post = get_post( get_the_ID() );
	$excerpt = $post->post_excerpt;
	$excerpt = strip_tags($excerpt);
	if ( !empty( $excerpt ) ) {
		return $excerpt;
	} else {
		$content = $post->post_content;
  	$length = 120;
  	$content = preg_replace('/<!--more-->.+/is', "", $content);
  	$content = strip_shortcodes($content);
  	$content = strip_tags($content);
  	$content = str_replace("&nbsp;", "", $content);
  	$content = mb_substr($content, 0, $length);
		return $content;
	}
}
endif;

// Sort by update date and time.
/*
if ( !function_exists( 'ampbase_orderby_modified' ) ):
function ampbase_orderby_modified( $query ) {
	if( $query->is_main_query() ) {
		if( $query->is_home() || $query->is_category() || $query->is_archive() || $query->is_tag() ) {
			$query->set( 'orderby', 'modified' );
		}
	}
}
add_action( 'pre_get_posts', 'ampbase_orderby_modified' );
endif;
*/

// Register widget.
if ( !function_exists( 'ampbase_widgets_init' ) ):
function ampbase_widgets_init() {
	register_sidebar( array(
		'name' 					=> 'Nav Widget',
		'id' 						=> 'nav-widget',
		'description' 	=> 'Nav Widget',
		'before_widget' => '<div>',
		'after_widget' 	=> '</div>',
		'before_title' 	=> '<h2>',
		'after_title' 	=> '</h2>'
	) );
	register_sidebar( array(
		'name' 					=> 'home main menu',
		'id' 						=> 'home-main-menu',
		'description' 	=> 'home main menu',
		'before_widget' => '<div>',
		'after_widget' 	=> '</div>',
		'before_title' 	=> '<h2>',
		'after_title' 	=> '</h2>'
	) );
	register_sidebar( array(
		'name' 					=> 'home nav widget',
		'id' 						=> 'home-nav-widget',
		'description' 	=> 'home nav widget',
		'before_widget' => '<div>',
		'after_widget' 	=> '</div>',
		'before_title' 	=> '<h2>',
		'after_title' 	=> '</h2>'
	) );
	register_sidebar( array(
		'name' 					=> 'footer menu',
		'id' 						=> 'footer-menu',
		'description' 	=> 'footer menu',
		'before_widget' => '<div>',
		'after_widget' 	=> '</div>',
		'before_title' 	=> '<h2>',
		'after_title' 	=> '</h2>'
	) );
	register_sidebar( array(
		'name' 					=> 'Singular Nav Top Ad',
		'id' 						=> 'singular-nav-top-ad',
		'description' 	=> 'Singular Nav Top Ad',
		'before_widget' => '<div>',
		'after_widget' 	=> '</div>',
		'before_title' 	=> '<h2>',
		'after_title' 	=> '</h2>'
	) );
	register_sidebar( array(
		'name' 					=> 'Singular Nav Top AMP Ad',
		'id' 						=> 'singular-nav-top-amp-ad',
		'description' 	=> 'Singular Nav Top AMP Ad',
		'before_widget' => '<div>',
		'after_widget' 	=> '</div>',
		'before_title' 	=> '<h2>',
		'after_title' 	=> '</h2>'
	) );
	register_sidebar( array(
		'name' 					=> 'Singular Nav Bottom Ad',
		'id' 						=> 'singular-nav-bottom-ad',
		'description' 	=> 'Singular Nav Bottom Ad',
		'before_widget' => '<div>',
		'after_widget' 	=> '</div>',
		'before_title' 	=> '<h2>',
		'after_title' 	=> '</h2>'
	) );
	register_sidebar( array(
		'name' 					=> 'Singular Nav Bottom AMP Ad',
		'id' 						=> 'singular-nav-bottom-amp-ad',
		'description' 	=> 'Singular Nav Bottom AMP Ad',
		'before_widget' => '<div>',
		'after_widget' 	=> '</div>',
		'before_title' 	=> '<h2>',
		'after_title' 	=> '</h2>'
	) );
	register_sidebar( array(
		'name' 					=> 'Category Nav Top Ad',
		'id' 						=> 'category-nav-top-ad',
		'description' 	=> 'Category Nav Top Ad',
		'before_widget' => '<div>',
		'after_widget' 	=> '</div>',
		'before_title' 	=> '<h2>',
		'after_title' 	=> '</h2>'
	) );
	/*
	register_sidebar( array(
		'name' 					=> 'Category Nav Top AMP Ad',
		'id' 						=> 'category-nav-top-amp-ad',
		'description' 	=> 'Category Nav Top AMP Ad',
		'before_widget' => '<div>',
		'after_widget' 	=> '</div>',
		'before_title' 	=> '<h2>',
		'after_title' 	=> '</h2>'
	) );
	*/
	register_sidebar( array(
		'name' 					=> 'Category Nav Bottom Ad',
		'id' 						=> 'category-nav-bottom-ad',
		'description' 	=> 'Category Nav Bottom Ad',
		'before_widget' => '<div>',
		'after_widget' 	=> '</div>',
		'before_title' 	=> '<h2>',
		'after_title' 	=> '</h2>'
	) );
	/*
	register_sidebar( array(
		'name' 					=> 'Category Nav Bottom AMP Ad',
		'id' 						=> 'category-nav-bottom-amp-ad',
		'description' 	=> 'Category Nav Bottom AMP Ad',
		'before_widget' => '<div>',
		'after_widget' 	=> '</div>',
		'before_title' 	=> '<h2>',
		'after_title' 	=> '</h2>'
	) );
	*/
}
add_action( 'widgets_init', 'ampbase_widgets_init' );
endif;

// Implement the Custom Header feature.
$ampbase_custom_header_defaults = array(
	'width'				=> 1200,
	'height'				=> 280,
	'header-text'		=> false
);
add_theme_support( 'custom-header', $ampbase_custom_header_defaults );

// Disable auto insert of <p> in articles with the specified ID.
if ( !function_exists( 'ampbase_disable_auto_insert' ) ):
function ampbase_disable_auto_insert( $content ) {
	$disable_all = get_theme_mod( 'disable_all_auto_insert', false );
	if ( $disable_all ) {
		remove_filter( 'the_content', 'wpautop' );
		remove_filter( 'the_excerpt', 'wpautop' );
	
	} else {
		global $post;
		$disable_ids = get_theme_mod( 'id_disable_auto_insert', false );
		$disable_ids = explode(",", $disable_ids);
		if ( $disable_ids ) {
			if ( in_array($post->ID, (array)$disable_ids) ) {
				remove_filter( 'the_content', 'wpautop' );
				remove_filter( 'the_excerpt', 'wpautop' );
				$content = str_replace(array("\r\n", "\r", "\n"), '', $content);
			}
		}
	}
	return $content;
}
endif;
add_filter( 'the_content', 'ampbase_disable_auto_insert', 5);

// Delete p tag wrapping img.
if ( !function_exists( 'ampbase_remove_p_wrapping_images' ) ):
function ampbase_remove_p_wrapping_images($content){
    //  $content = preg_replace('/<p>(\s*)(<(a|img) .*(\/a| \/)>)(\s*)<\/p>/iU', '\2', $content);
    // $content = preg_replace('/<p>(\s*)(<amp-img .*><\/amp-img>)(\s*)<\/p>/iU', '\2', $content);

    $content = preg_replace('/<p>(\s*)(<img .* \/>)(\s*)<\/p>/iU', '\2', $content);
    $content = preg_replace('/<p>(\s*)(<a href=".*"><img .* \/><\/a>)(\s*)<\/p>/iU', '\2', $content);
		return $content;
}
endif;
add_filter( 'the_content', 'ampbase_remove_p_wrapping_images', 20);

// Allow svg file.
/*
function ampbase_custom_mime_types( $mimes ) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter( 'upload_mimes', 'ampbase_custom_mime_types' );
*/

// Enable custom background.
add_theme_support( 'custom-background' );

// Add callback for custom TinyMCE editor stylesheets.
add_editor_style();

/**
 * Tests if any of a post's assigned categories are descendants of target categories
 *
 * @param int|array $cats The target categories. Integer ID or array of integer IDs
 * @param int|object $_post The post. Omit to test the current post in the Loop or main query
 * @return bool True if at least 1 of the post's categories is a descendant of any of the target categories
 * @see get_term_by() You can get a category by name or slug, then pass ID to this function
 * @uses get_term_children() Passes $cats
 * @uses in_category() Passes $_post (can be empty)
 * @version 2.7
 * @link http://codex.wordpress.org/Function_Reference/in_category#Testing_if_a_post_is_in_a_descendant_category
 */
if ( ! function_exists( 'post_is_in_descendant_category' ) ) {
	function post_is_in_descendant_category( $cats, $_post = null ) {
		foreach ( (array) $cats as $cat ) {
			// get_term_children() accepts integer ID only
			$descendants = get_term_children( (int) $cat, 'category' );
			if ( $descendants && in_category( $descendants, $_post ) )
				return true;
		}
		return false;
	}
}
