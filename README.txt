=== AMPBase ===
Contributors: Hidetoshi Fukushima
Requires at least: WordPress 4.8.2
Tested up to: WordPress 5.2.4
Version: 1.2.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: right-sidebar, theme-options, blog

==der image correspondence with amp, content width adjustment, etc. Description ==

"AMPBase" is a theme aimed at using as a base when creating a theme compatible with AMP. However, you can use it as it is. For ease of use as a base, we try to make it as easy to understand as possible, short code. Regarding the implementation of the function, we are not aiming to cover it, but we focus on the minimum necessary functions.

== Installation ==

1. In your admin panel, go to Appearance -> Themes and click the 'Add New' button.
2. Type in ampbase in the search form and press the 'Enter' key on your keyboard.
3. Click on the 'Activate' button to use your new theme right away.
4. Navigate to Appearance > Customize in your admin panel and customize.

== Copyright ==

AMPBase, Copyright 2017 Hidetoshi Fukushima
AMPBase is distributed under the terms of the GNU GPL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

AMPBase bundles the following third-party resources:

Simplicity2
License: GPLv2 or later
https://wp-simplicity.com/

Reset CSS v2.0
License: none (public domain)
http://meyerweb.com/eric/tools/css/reset/

== Changelog ==

= 1.2.4 =
* Released: October 27, 2019

Fixed bugs that caused the display to protrude sideways when using the <pre>.

= 1.2.3 =
* Released: October 24, 2018

Adjust the margin of <ol><li>

= 1.2.2 =
* Released: September 27, 2018

Apply "font-style: italic;" to <i> and <em>

= 1.2.1 =
* Released: April 19, 2018

Deleted "max-height: 100%;" specification for <img> and <amp-img>.

= 1.2.0 =
* Released: April 4, 2018

Added "post_is_in_descendant_category" function. When specifying a category to disable AMP, you only need to specify parent category.

= 1.1.9 =
* Released: March 14, 2018

Only single page is converted to AMP.

= 1.1.8 =
* Released: March 7, 2018

Output structured markup only for single pages.

= 1.1.7 =
* Released: March 2, 2018

Add style of <sup> tag.

= 1.1.6 =
* Released: February 26, 2018

Resolve issues that do not markers on Google Maps.

= 1.1.5 =
* Released: February 23, 2018

By default, AMP is enabled for all categories. If there are categories you do not want to use AMP, specify individually.

= 1.1.4 =
* Released: February 15, 2018

Changed ol tag style from disc to decimal.

= 1.1.3 =
* Released: December 23, 2017

Ads can be posted even outside the article page.

= 1.1.2 =
* Released: December 13, 2017

Fixed bug corresponding to json-ld.

= 1.1.1 =
* Released: December 6, 2017

Enable to specify ID to disable automatic insertion of <p>.

= 1.1.0 =
* Released: November 8, 2017

Show past postings in one column.

= 1.0.9 =
* Released: November 3, 2017

I made it possible to put a thumbnail-included article list in the menu.

= 1.0.8 =
* Released: October 20, 2017

Delete the p tag that wraps the image inserted by WordPress.
Make the entire frame clickable with the menu.

= 1.0.7 =
* Released: October 13, 2017

Menu widgetization.
Changing header design.

= 1.0.6 =
* Released: October 6, 2017

Corresponding to amp of header image display etc.

= 1.0.5 =
* Released: October 3, 2017

Disable the css minify function.
Enable amp-iframe compatibility.

= 1.0.4 =
* Released: September 29, 2017

For PHP 5.2.x, do not use namespace.

= 1.0.3 =
* Released: September 27, 2017

Add tracking code setting of Google Analitics.

= 1.0.2 =
* Released: September 26, 2017

Initial release
